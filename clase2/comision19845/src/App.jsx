import RoutesApp from "./routes/RoutesApp"
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

function App() {    
  
    
    return (
        <div
            //onClick={() => alert('soy evento de app')} 
            className="App border border-3 border-success">
            <RoutesApp />
        </div>
  )
}

export default App
// className="border border-1 border-warning"