import { memo } from 'react'
import Item from "../Item"


// memo(fn) fn= componente // memo(fn, FN) FN= funcion comparadora


const ItemList = memo(
    ({ productos }) => {
        console.log('itemList')
        return (
            <>
                {productos.map((producto)=>   <Item key={producto.id} producto={producto} /> )}
            </>
        )
    }  
  , ( viejaProp, nuevaProp )=> viejaProp.productos.length === nuevaProp.productos.length)

export default ItemList