
import { useCartContext } from "../../context/CartContext"


import ItemCount from "../ItemCount/ItemCount"
import { ControlledInput, LoadingComponent, TextComponent, TextComponent2, TextComponent3, TextComponent4, TextComponent5, TextComponent6, TextComponent7 } from "../../clases/clase11/ComponenteEjemplosCondicionales"


function ItemDetail({ producto }) {

  const {addToCart, cartList} = useCartContext()

  // estado
  function onAdd(cant) {
    console.log(cant)
    addToCart( { ...producto, cantidad: cant } )
  }

  console.log(cartList)
  return (
    <>
      <img src={producto.foto} className="w-25" />
      <div>{producto.name}</div>
      <div>{producto.price}</div>
      {/* <div>{producto.descripcion}</div> */}
      <ItemCount initial={1}  stock={10} onAdd={onAdd} />


      {/* <ControlledInput /> */}
      {/* <LoadingComponent /> */}
      {/* <TextComponent6 otro='mt-2'  /> */}
      {/* <TextComponent7 /> */}
    </>
  )
}
export default ItemDetail
