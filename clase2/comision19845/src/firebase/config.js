
import { initializeApp } from "firebase/app";


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCMQPcFx2B5c4Qu_hA9nQHjrYpG-1-5uX8",
  authDomain: "comision19845.firebaseapp.com",
  projectId: "comision19845",
  storageBucket: "comision19845.appspot.com",
  messagingSenderId: "343497572618",
  appId: "1:343497572618:web:d71696a1d8f801be47faf3"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig)

export const getFirestoreApp=()=>{ 
    return app
}