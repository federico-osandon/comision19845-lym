import Titulo from "../Titulo/Titulo";

export default function Input( {placeholder, saludo} ){
    return(
        <>
            <Titulo tituloProps={ 'titulo de input' } subTit='Soy subtitulo de input' />
            <input placeholder={ placeholder } />
            <button onClick={saludo}>SAludo</button>
        </>
    )
}