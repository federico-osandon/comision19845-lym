import { useState } from 'react'
import ItemListContainer from '../../container/ItemListContainer/ItemListContainer'
import Input from '../components/Input/Input'
import NavBar from '../components/NavBar/NavBar'
import Titulo from '../components/Titulo/Titulo'

    function RoutesApp() {
        
        const [count, setCount] = useState(0)
        
            const style = { backgroundColor: 'blue' } // estados
        
            const handleConsole = () => {
                console.log('soy evento')
            }
        
            let titulo = 'Soy Titulo de app' // estado
        
            const fnSaludo =  () => {
                console.log('saludando')
            }


      return (
        < >
            <NavBar  />             
            <Titulo 
                tituloProps={ titulo }
                subTit='Soy subtitulo de app' 
            /> 
            <ItemListContainer />
        </>
      )
    }
    
    export default RoutesApp