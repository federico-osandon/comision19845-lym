import { useState, useEffect } from 'react'

function ItemListContainer({greeting, titulo}) {
    const [ count, setCount ] = useState( 0 )
    const [ bool, setBool ] = useState( true )
    //let count = 0

    useEffect(()=>{
        //acciones 
        console.log('SE ejecuta siempre 1')
        //addEventListener
        return ()=>{
            console.log('efecto de limpieza romove funcionabilidad')
        }
    })

    useEffect(()=>{ // una sola vez despues del montado del componente
        //acciones 
        console.log('llama a api 2')
    }, [])

    useEffect(()=>{ // una sola vez despues del montado del componente
        //acciones 
        alert('Solo cuando cambie bool 3')
    }, [bool, count])

    const manejarCount =()=>{
        // count=count+1
        // count ++
        // count += 1
        // console.log(count)
        setCount( count + 5  )
    }
    console.log('montado y rendrizado 4')
    return (
        <div>
            {greeting}<hr />
            {/* <OtroComponnet /> */}
            <label > {count} </label><br/>
            <button onClick={manejarCount}>click</button>
            <button onClick={()=> setBool(!bool)}>click bool</button>
            {/* { titulo( { tituloProps: 'soy titulo', subTit: 'soy titulo'} ) } */}
        </div> 
    )
}

export default ItemListContainer