import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import ItemList from '../../components/ItemList/ItemList'
import { getFetch } from '../../helpers/getFetch'
import { collection, doc, getDoc, getDocs, getFirestore, limit, query, where } from 'firebase/firestore'


function ItemListContainer({greeting, titulo}) {

    const [productos, setProductos] = useState([])
    const [producto, setProducto] = useState({})
    const [loading, setLoading] = useState(true)
    const [bool, setBool] = useState(true)
    //const [items, setItems] = useState([])

    // hook de react router dom
    const { categoriaId } = useParams()

// usar en itemDetailContainer
    // useEffect(()=>{
    //     const querydb = getFirestore() 
    //     const queryProd = doc(querydb, 'productos', '4jNlWgWGlGSO7WGASegG')

    //     getDoc(queryProd)
    //     .then(resp => setProducto(  { id: resp.id, ...resp.data() }  ))
    // }, [])
    
    // para traer todos
    // useEffect(()=>{
    //     const querydb = getFirestore() 
    //     const queryCollection = collection(querydb, 'productos')

    //     getDocs(queryCollection)
    //     .then(resp => setProductos( resp.docs.map(item => ( { id: item.id, ...item.data() } ) ) ))
    //     .catch(err => console.log(err))
    //     .finally(() => setLoading(false)) 
    // }, [])

    useEffect(()=>{
        const querydb = getFirestore() 
        const queryCollection = collection(querydb, 'productos')
        const queryFilter = query(queryCollection, 
            // where('price','>=', 1500)
            where('categoria','==', 'gorras'),            
            limit(1)
            // orderBy('name', 'asc')
        )

        getDocs(queryFilter)
        .then(resp => setProductos( resp.docs.map(item => ( { id: item.id, ...item.data() } ) ) ))
        .catch(err => console.log(err))
        .finally(() => setLoading(false)) 
    }, [])

    console.log(productos)
    // useEffect(()=> {
    //     if (categoriaId) {
    //         getFetch // funcion que simula una api
    //         // .then(resp =>{ 
    //         //     //throw new Error('ESto es un error')//insatanciando un error
    //         //     console.log(resp)
    //         //     return resp
    //         // })
    //         .then(resp => setProductos(resp.filter(item => item.categoria === categoriaId)) )
    //         .catch(err => console.log(err))
    //         .finally(() => setLoading(false))            
            
    //     } else {            
    //         getFetch // funcion que simula una api
    //         // .then(resp =>{ 
    //         //     //throw new Error('ESto es un error')//insatanciando un error
    //         //     console.log(resp)
    //         //     return resp
    //         // })
    //         .then(resp => setProductos(resp) )
    //         .catch(err => console.log(err))
    //         .finally(() => setLoading(false))            
    //     }
    // }, [categoriaId])

    // const getFetchApi = async () => {
    //     try {
    //         const query = await fetch('/assets/DATA.json')// por defecto va un verbo tipo get
    //         const queryParse = await query.json()
    //         console.log(queryParse)
    //     } catch (error) {
    //         console.log(error)
    //     }
        
    // }


    //como usar la función fetch
    // useEffect(()=>{
    //     fetch('/assets/DATA.json')// por defecto va un verbo tipo get
    //     .then(data => data.json())
    //     .then(resp => console.log(resp))
    // }, [])

    // useEffect( ()=>{
    //     getFetchApi()
    // }, [])

      
   //ejemplo de evento
   const handleClick=(e)=>{
        e.preventDefault() 
        setBool(!bool)
    }

    const handleAgregar=()=>{
        setProductos([
            ...productos,
            { id: "8", name: "Gorra 7", url: 'https://www.remerasya.com/pub/media/catalog/product/cache/e4d64343b1bc593f1c5348fe05efa4a6/r/e/remera_negra_lisa.jpg', categoria: "remera" , price: 2 }
        ])
    }


    
    return (
        <div>
            {greeting}<hr />
            <button onClick={handleClick}>Cambiar estado </button>           
            <button onClick={handleAgregar}>Agregar Item </button>
            {/* [1,2,3] => [<li>1</li>,<li>2</li>...] */}
            {/* { [1,2,3,4].map((nro)=> <li key={nro} >{nro}</li> ) }           */}
            {   loading ? 
                    <h2>Cargando...</h2>
                : 
                    <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                        <ItemList productos={productos} />
                    </div>                   
            
            }          

        </div> 
    )
}

export default ItemListContainer