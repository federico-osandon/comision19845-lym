import { doc, getDoc, getFirestore } from 'firebase/firestore'
import {useEffect, useState} from 'react'
import Caso1 from '../../clases/clase9/Caso1'
import Caso2 from '../../clases/clase9/Caso2'
import { Input } from '../../clases/clase9/Input'
import Intercambiabilidad from '../../clases/clase9/Intercambiabilidad'
import ItemDetail from "../../components/ItemDetail/ItemDetail"
import { getFetch, getFetchOne } from '../../helpers/getFetch'

function ItemDetailContainer() {
    const [producto, setProducto] = useState( {} )
    const [loading, setLoading] = useState( true )

    // usar useParam => detalleId

    useEffect(()=>{
        const querydb = getFirestore() 
        const queryProd = doc(querydb, 'productos', '4jNlWgWGlGSO7WGASegG')

        getDoc(queryProd)
        .then(resp => setProducto(  { id: resp.id, ...resp.data() }  ))
        .catch(err => console.log(err))
        .finally(() => setLoading(false))
    }, [])

  return (
        <div className="border border-3 border-primary">
            {/* <Input /> */}
            { loading ? <h2>Loading...</h2> : 
              <ItemDetail producto={producto} />
              }
            {/* <Intercambiabilidad/> */}
            {/* <Caso1 /> */}

            {/* <Caso2 /> */}
        </div>
  )
}

export default ItemDetailContainer